const app = require("express")();
const server = require("http").createServer(app);
const cors = require("cors");
const mysql = require("mysql")

const io = require("socket.io")(server, {
	cors: {
		origin: "*",
		methods: ["GET", "POST"]
	}
});

var con = mysql.createConnection({

	host: "localhost",
	user: "root",
	password: "",
	database: "project4"

});

app.use(cors());
const arrUserInfo = [];
const status = ''
const PORT = process.env.PORT || 5000;

app.get('/', (req, res) => {
	res.send('Server running');
});

io.on("connection", (socket) => {
	console.log(socket.id)
	console.log("conn")


	socket.emit("me", socket.id);

	socket.on('sendUser', user => {
		console.log(user)
		arrUserInfo.push(user);
		socket.emit('DANH_SACH_ONLINE', arrUserInfo);
	})
	socket.on("disconnect", () => {
		socket.broadcast.emit("callEnded")
	});


	socket.on("call", ({ userToCall, from }) => {

		io.to(userToCall).emit("callUser", { from });

	});

	socket.on("callUser", ({ userToCall, signalData, from, name }) => {
		console.log('signal', signalData, 'from', from);

		io.to(userToCall).emit("callUser", { signal: signalData, from });

	});

	socket.on("answerCall", (data) => {
		console.log("len", data)
		io.to(data.to).emit("callAccepted", data.signal)
	});

	socket.on("sendMessage", (data)=>{
		console.log(data)
		con.connect(function(err) {
			if (err) throw err;
			var sql = "INSERT INTO message (`id`, `Data`, `SenderId`, `ReceiverId`, `Create_At`, `Isreader`, `SendingTime`) VALUES (NULL, '"+data.Data+"', '"+data.SenderId+"', '"+data.ReceiverId+"', 'current_timestamp()', NULL, NULL);";
			con.query(sql, function(err, results) {
			  if (err) throw err;
			  console.log(results);
			})
		  });
		io.to(data.usertosend).emit("receiverMessage", data);
	})
});

server.listen(PORT, () => console.log(`Server is running on port ${PORT}`));
