import axiosClient from "./axiosClienr";

const CustomerApi = {
  getMessage: (userid, friendid) => {
    const url = `/getMessage/${userid}&${friendid}`;
    return axiosClient.get(url);
  },

  getInfor: (userid, friendid) => {
    const url = `/Customer/GetInfor/${userid}&&${friendid}`;
    return axiosClient.get(url);
  },
  getInforFriend: (idUser, idFriend) => {
    const url = `/getInforfriend/${idUser}&${idFriend}`;
    return axiosClient.get(url);
  },
  acceptFriend: (params) => {
    const url = `/Customer/AcceptFriend`;
    return axiosClient.post(url, params);
  },
  getInforuser: (userid) => {
    const url = `/Customer/GetCustomer/${userid}`;
    return axiosClient.get(url);
  },
  getFriendAdd: (userid) => {
    const url = `/Customer/GetFriendAdd/${userid}`;
    return axiosClient.get(url);
  },
  helpGetFriend: (userid) => {
    const url = `/Customer/HelpGetFriend/${userid}`;
    return axiosClient.get(url);
  },
  loginUser: (params) => {
    const url = "/Login/LoginUser";
    return axiosClient.post(url, {
      params,
    });
  },
};

export default CustomerApi;
