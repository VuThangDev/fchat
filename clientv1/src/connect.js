import { io } from 'socket.io-client';
const socket = io('http://localhost:5000');
socket.on('me', id => localStorage.setItem('socketId', id))
console.log('id', localStorage.getItem('socketId'))
export default socket;