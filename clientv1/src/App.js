import "./App.css";
import React, { useEffect, lazy, Suspense, useState } from "react";
import CustomerApi from "./api/CustomerApi";

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  // Router,
  Redirect,
  BrowserRouter,
} from "react-router-dom";
import SidebarChat from "./features/chat/sidebar/SidebarChat.bak";
import SidebarFriend from "./features/chat/sidebar/SidebarFriend";
import SidebarFavourite from "./features/chat/sidebar/SidebarFavourite";
import AddUser from "./features/chat/typing/AddUser";
import ChatTyping from "./features/chat/typing/TypingChat";
import SlideShow from "./features/chat/typing/SlideShow";
import SlideShowUs from "./features/chat/typing/SlideShowUs";
import Chatdefault from "./features/chat";
import LoginUser from "./features/login/Login.bak";
import Registers from "./features/login/Register";
import Setting from "./features/chat/Popup/Setting";
import LoginPageAdmin from "./features/admin/Login";
import Dashboad from "./features/admin/Dashboad";
//import videochat from "./features/videoChat";
import video from "./features/video";
//import videochat from "./features/video1";
//import videochat from "./features/videoChat";
//import { ContextProvider } from "./features/videoChat/Context";
//import Notifications from "./features/videoChat/components/Notifications";

const login = React.lazy(() => import("./features/login"));
// const chat = React.lazy(() => import("./features/chat"));

function App() {
  const [checkLogged, setCheckLogged] = useState(false);
  useEffect(() => {   
    if (localStorage.getItem('@login')) {

    }
  }, [])
  return (
    <Suspense fallback={<div>loading ...</div>}>
      <Router>
        
        <Route exact path="/logins" component={LoginUser} />
        <Route exact path="/loginadmin" component={LoginPageAdmin} />

        <Route path="/register" component={Registers} />        
        <Route path="/video" component={video} />                  
        <Route exact path="/dashboad" component={Dashboad} />

        <Route path="/c/:idchat">
          <Chatdefault />
          <Route path="/c/:idchat/chat">
            <div id="content">
              <div className="sidebar-group">
                <Route component={SidebarChat} />
              </div>
              <Route path="/c/:idchat/chat/slide" component={SlideShow} />
              <Route
                path="/c/:idchat/chat/message/:idfriend"
                component={ChatTyping}
              />
            </div>
          </Route>
          <Route path="/c/:id/friend">
            <div id="content">
              <div className="Sidebar-Friend">
                <Route component={SidebarFriend} />
              </div>
              <Route component={AddUser} />
            </div>
          </Route>
          <Route path="/c/:id/favourite">
            <div id="content">
              <div className="sidebar-group">
                <Route component={SidebarFavourite} />
              </div>
              <Route component={SlideShowUs} />
            </div>
          </Route>
        </Route>

        {/* </Route> */}
      </Router>
    </Suspense>
  );
}

export default App;
