import React, { useEffect, useState } from 'react';
import Axios from 'axios';
import "./dist/css/login2.css"
import { io } from 'socket.io-client';
const LoginPage = (props) => {
    const [state, setState] = useState({
        Email: '',
        Password: '',
    })
    //const [sockets, setSockets] = useState('');  
    useEffect(() => {
        if (localStorage.getItem('@login')) {
            const newState = JSON.parse(localStorage.getItem('@login'))
            setState(newState)
        }
    }, [])

    const handlleLogin = () => {
        Axios.post("http://127.0.0.1:8000/api/login", { ...state }).then(
            res => res.data
        ).then(res => {
            var id = res.datas[0].Id;
            if (res.Status == "Invalid") alert("Invalid User");
            else {
                //console.log('socketId', localStorage.getItem('socketId'));
                props.history.replace(`/c/${id}/chat/slide`)
                localStorage.setItem('@currentUser', id)
                localStorage.setItem('@login', JSON.stringify({
                    Email: state.Email,
                    Password: state.Password,
                }))
            };
        });
    }
    const handleChangePassword = e => {
        setState({ ...state, Password: e.target.value });
    }
    const handleChangeUsername = e => {
        setState({ ...state, Email: e.target.value });
    }
    return (
        <div className="form-membership">
            <div className="form-wrapper">
                {/* logo */}
                <div className="logo">
                    <svg
                        version="1.1"
                        id="Capa_1"
                        xmlns="http://www.w3.org/2000/svg"
                        xmlnsXlink="http://www.w3.org/1999/xlink"
                        x="0px"
                        y="0px"
                        width="50.004px"
                        height="50.003px"
                        viewBox="0 0 33.004 33.003"
                        style={{ enableBackground: "new 0 0 33.004 33.003" }}
                        xmlSpace="preserve"
                    >
                        <g>
                            <path
                                d="M4.393,4.788c-5.857,5.857-5.858,15.354,0,21.213c4.875,4.875,12.271,5.688,17.994,2.447l10.617,4.161l-4.857-9.998
                    c3.133-5.697,2.289-12.996-2.539-17.824C19.748-1.072,10.25-1.07,4.393,4.788z M25.317,22.149l0.261,0.512l1.092,2.142l0.006,0.01
                    l1.717,3.536l-3.748-1.47l-0.037-0.015l-2.352-0.883l-0.582-0.219c-4.773,3.076-11.221,2.526-15.394-1.646
                    C1.469,19.305,1.469,11.481,6.277,6.672c4.81-4.809,12.634-4.809,17.443,0.001C27.919,10.872,28.451,17.368,25.317,22.149z"
                            />
                            <g>
                                <circle cx="9.835" cy="16.043" r="1.833" />
                                <circle cx="15.502" cy="16.043" r="1.833" />
                                <circle cx="21.168" cy="16.043" r="1.833" />
                            </g>
                        </g>
                        <g />
                        <g />
                        <g />
                        <g />
                        <g />
                        <g />
                        <g />
                        <g />
                        <g />
                        <g />
                        <g />
                        <g />
                        <g />
                        <g />
                        <g />
                    </svg>
                </div>
                {/* ./ logo */}
                <h5>Fchat</h5>
                <div id="title-head">
                    <div>Liên hệ với mọi người trong cuộc sống của bạn</div>
                    <div>đăng nhập ngay để bắt đầu</div>
                </div>
                <div id="title-error">
                    <div>Sai tên tài khoản hoặc mật khẩu</div>
                    <div>
                        Vui lòng nhập lại thông tin.{" "}
                        <a href="reset-password.html">Quên mật khẩu?</a>
                    </div>
                </div>
                {/* form */}
                <div className="form-group input-group-lg">
                    <input
                        type="text"
                        id="username"
                        className="form-control"
                        placeholder="Username or email"
                        value={state.Email}
                        onChange={handleChangeUsername}
                        required
                        autofocus
                    />
                </div>
                <div className="form-group input-group-lg">
                    <input
                        type="password"
                        id="password"
                        value={state.Password}
                        className="form-control"
                        placeholder="Password"
                        onChange={handleChangePassword}
                        required
                    />
                </div>
                <div className="form-group d-flex justify-content-between">
                    <a href="reset-password.html" className="aCard">Reset password</a>
                </div>
                <div className="btn-primary btn-lg btn-block">
                    <button onClick={handlleLogin} className="btnSubmit">Sign in</button>
                </div>

                <p className="text-muted">Don't have an account?</p>
                <a href="register.html" className="btn  btn-sm">
                    Register now!
                </a>
            </div>
        </div>
    );
}

export default LoginPage;