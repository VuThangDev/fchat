import React, { useContext } from 'react';
import { Grid, Typography, Paper, makeStyles } from '@material-ui/core';

import { SocketContext } from '../Context';

const useStyles = makeStyles((theme) => ({
  video: {
    width: '550px',
    [theme.breakpoints.down('xs')]: {
      width: '300px',
    },
  },
  gridContainer: {
    justifyContent: 'center',
    [theme.breakpoints.down('xs')]: {
      flexDirection: 'column',
    },
  },
  paper: {
    padding: '10px',
    border: '2px solid black',
    margin: '10px',
  },
}));

const VideoPlayer = () => {
  const { name, callAccepted, myVideo, userVideo, callEnded, stream, call, callAccept } = useContext(SocketContext);
  const classes = useStyles();

  return (
    <div id="video">
      <div className="myvideo">
        {stream && (
          <video muted ref={myVideo} autoPlay className="myVideo" />
        )}
      </div>
      {callAccepted && !callEnded ? (
        <div>
          <div className="uservideo">
            <video playsInline ref={userVideo} autoPlay className="audioVideo" />
          </div>

        </div>
      ) : !callAccepted && callAccept ?(
        <div className="uservideoCall">
          <div>
            <div>
              <img src="./images/1.jpg" alt="anh" className="imgCall" />
            </div>
            <p className="nameCall">Vu Van Thang</p>
          </div>
          <div>
            <p>Đang gọi...</p>
          </div>
        </div>
      ): !callAccepted && !callAccept &&(
        <div className="uservideoCall">
          <div>
            <div>
              <img src="./images/1.jpg" alt="anh" className="imgCall" />
            </div>
            <p className="nameCall">Vu Van Thang</p>
          </div>
          <div>
            <p>Ket noi voi</p>
          </div>
        </div>
      )}
    </div>
  );
};

export default VideoPlayer;
