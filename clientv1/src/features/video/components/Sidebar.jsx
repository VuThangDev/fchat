import React, { useState, useContext, useEffect } from 'react';
import { Button, TextField, Grid, Typography, Container, Paper } from '@material-ui/core';
import { CopyToClipboard } from 'react-copy-to-clipboard';
import { Assignment, Phone, PhoneDisabled } from '@material-ui/icons';
import { makeStyles } from '@material-ui/core/styles';
import socket from '../../../connect';

import { SocketContext } from '../Context';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
  },
  gridContainer: {
    width: '100%',
    [theme.breakpoints.down('xs')]: {
      flexDirection: 'column',
    },
  },
  container: {
    width: '600px',
    margin: '35px 0',
    padding: 0,
    [theme.breakpoints.down('xs')]: {
      width: '80%',
    },
  },
  margin: {
    marginTop: 20,
  },
  padding: {
    padding: 20,
  },
  paper: {
    padding: '10px 20px',
    border: '2px solid black',
  },
}));

const Sidebar = ({ children }) => {
  const { me, callAccepted, name, setName, callEnded, leaveCall, callUser, callAccept } = useContext(SocketContext);
  const [idToCall, setIdToCall] = useState('');
  const classes = useStyles();

  useEffect(() => {
    setName('Thang');
    setIdToCall(me);
  }, [me]);
  return (
    <div>
      {callAccepted && !callEnded ? (
        <div className="callContainer">
          <div>
            <Button variant="contained" color="secondary" startIcon={<PhoneDisabled fontSize="large" />} onClick={leaveCall} className='btncall'>
              Huỷ
            </Button>
          </div>
        </div>
      ) : !callAccepted && callAccept ? (
        <div className="callContainer">
          <div>
            <Button variant="contained" color="secondary" startIcon={<PhoneDisabled fontSize="large" />} onClick={leaveCall} className='btncall'>
              Huỷ
            </Button>
          </div>
        </div>
      ) : (
        <div className="callContainer">
          <div>
            <Button variant="contained" color="primary" startIcon={<Phone fontSize="large" />} onClick={() => { socket.emit('call', { userToCall: localStorage.getItem('userSocket'),  from: me }); console.log('...') }} className='btncall'>
              Gọi
            </Button>
            <Button variant="contained" color="secondary" startIcon={<PhoneDisabled fontSize="large" />} onClick={leaveCall} className='btncall'>
              Huỷ
            </Button>
          </div>
        </div>
      )
      }
    </div>
  );
};

export default Sidebar;
