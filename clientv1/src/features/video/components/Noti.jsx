import React, { useContext } from 'react';
import { Button } from '@material-ui/core';

import { SocketContext } from '../Context';

const Noti = () => {
  const { answerCall, call, callAccepted, me, callUser, noti } = useContext(SocketContext);

  return (
    <>
      { noti && !callAccepted && (
        <div style={{ display: 'flex', justifyContent: 'space-around', color: 'white'}}>
          <h1>{call.name} Chấp nhận cuộc gọi:</h1>
          <Button variant="contained" color="primary" onClick={()=>{callUser(localStorage.getItem('callers'))}}>
            dong y
          </Button>
        </div>
      )}
    </>
  );
};

export default Noti;
