import React, { useEffect, useState } from 'react';
import Axios from 'axios';
import "./admin.css";
import Infomation from './Infomation';
const Dashboad = (props) => {
    const [state, setState] = useState(        
        [])
    
        useEffect(() => {
           Axios.get("http://127.0.0.1:8000/api/statistical")
           .then(res=>res.data)
           .then(res=>{
            setState(res);
            console.log(state);
           })
        }, [])

    return (
        <div id="wraper">
        <div id="sidebar">
          <div id="title-form">
            ADMIN
          </div>
          <div className="sidebar-group-head">
            <div className="img-sidebar">
              <img src="anh/dz.jpg" alt="" className="img-sb" />
            </div>
            <div className="name">
              <p>Vu Thang</p>
              <a href="#">
                <i className="fas fa-circle" />
                online
              </a>
            </div>
          </div>
          <div className="title-sidebar">
            hành động chính
          </div>
          <div className="sidebar-item">
            <a href="#">
              <div className="action" />
              <i className="fas fa-tachometer-alt i-l" />
              <span style={{float: 'left'}}>Dashboard</span>
            </a>
          </div> 
          <div className="sidebar-item">
            <div className="sidebar-item main-sidebar">              
              <div className="remove" />
              <i className="fas fa-tachometer-alt i-l" />
              <span style={{float: 'left', paddingRight: '60px'}}>Quản lý người dùng</span>
              <div className="i-r">
                <i className="fas fa-angle-down d" />    
                <i className="fas fa-angle-left l" style={{display: 'none'}} />                        
              </div>
            </div> 
                                         
          </div>
        </div>
        <Infomation propsdad={state} />
        
      </div>
    );
}

export default Dashboad;