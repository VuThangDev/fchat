import React, { useEffect, useState } from 'react';
import Axios from 'axios';
import "./admins.css"


const Infomation = (props) => {
    
    console.log(props.propsdad)

    return (
        <div id="contentAdmin">
        <div id="content-header">
          <div id="header-left">
            <i className="fas fa-bars" />
          </div>
          <div id="header-right">                    
            <ul>
              <li>
                <a href className="icon">
                  <i className="far fa-envelope" />
                  <span className="label">2</span>
                </a>
              </li>
              <li>
                <a href className="icon">
                  <i className="far fa-bell" />
                  <span className="label">10</span>
                </a>
              </li>
              <li>
                <a href="#" className="name-header">
                  <img src="anh/dz.jpg" alt="" className="img-side" />
                  <span>Vu Thang</span>
                </a>
              </li>
            </ul>
          </div>
        </div>            
        <div id="content-main">
          <header>
            <div id="head-main-left">
              Thống kê
            </div>
            <div id="head-main-right">
              <i className="fas fa-tachometer-alt i-l" />
              <span><b> Home </b></span> &gt; <span> <b> Quản lý người dùng </b></span> &gt; Thông tin người dùng
            </div>
          </header>
          <div id="content-dash">            
            
          <div className="row">
        <div className="col-md-3 col-sm-6 col-xs-12">
          <div className="info-box">
            <span className="info-box-icon bg-aqua"><i class="fas fa-users"></i></span>
            <div className="info-box-content">
              <span className="info-box-text">User</span>
              <span className="info-box-number">{props.propsdad.user}</span>
            </div>
            {/* /.info-box-content */}
          </div>
          {/* /.info-box */}
        </div>
        {/* /.col */}
        <div className="col-md-3 col-sm-6 col-xs-12">
          <div className="info-box">
            <span className="info-box-icon bg-red"><i class="fas fa-user-shield"></i></span>
            <div className="info-box-content">
              <span className="info-box-text">Admin</span>
              <span className="info-box-number">{props.propsdad.admin}</span>
            </div>
            {/* /.info-box-content */}
          </div>
          {/* /.info-box */}
        </div>
        {/* /.col */}
        {/* fix for small devices only */}
        <div className="clearfix visible-sm-block" />
        <div className="col-md-3 col-sm-6 col-xs-12">
          <div className="info-box">
            <span className="info-box-icon bg-green"><i class="fab fa-facebook-messenger"></i></span>
            <div className="info-box-content">
              <span className="info-box-text">Message</span>
              <span className="info-box-number">{props.propsdad.message}</span>
            </div>
            {/* /.info-box-content */}
          </div>
          {/* /.info-box */}
        </div>
        {/* /.col */}
        <div className="col-md-3 col-sm-6 col-xs-12">
          <div className="info-box">
            <span className="info-box-icon bg-yellow"><i class="fas fa-user-plus"></i></span>
            <div className="info-box-content">
              <span className="info-box-text">New Members</span>
              <span className="info-box-number">2,000</span>
            </div>
            {/* /.info-box-content */}
          </div>
          {/* /.info-box */}
        </div>
        {/* /.col */}
      </div>
          </div>
        </div>
      </div>
    );
}

export default Infomation;