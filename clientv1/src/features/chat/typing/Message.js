import React from "react";

const Message = (props) =>
  props.rendermessageId == -2 ? (    
    <div className="message-item">
      <div className="message-reciver">
      <img src={props.img} alt="" className="imgMessage"></img>
      <div className="message-content">{props.datas}</div>
      </div>
      <div className="message-action"></div>
    </div>
  ) : (    props.isReader==1?(<div className="message-item outgoing-message">
  <div className="message-content">{props.datas}</div>
  <div className="message-action">Đã xem</div>
  </div>
  ):(<div className="message-item outgoing-message">
    <div className="message-content">{props.datas}</div>
    <div className="message-action"></div>
  </div>)
    
  );

export default Message;
