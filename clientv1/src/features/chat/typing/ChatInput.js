import React, { useEffect, useRef, useState } from "react";

const ChatInput = (props) => {
  const [message, setMessage] = useState("");


  const onSubmit = (e) => {
    e.preventDefault();
    // console.log(message)
    const isMessageProvided = message && message !== "";

    if (isMessageProvided) {
      props.sendMessage(message, null);
    } else {
      alert("Please insert an user and a message.");
    }
    setMessage("");
  };

  const onMessageUpdate = (e) => {
    setMessage(e.target.value);
  };

  return (
    <form
      onSubmit={onSubmit}
      style={{ backgroundColor: "#F0EEEE" }}
      className="frm"
      autoComplete="off"
    >
      <input        
        type="text"
        id="form-coltrol-chat"
        placeholder="Nhập văn bản"
        onChange={onMessageUpdate}
        value={message}
      />
      <button className="btn-chat">
      <i class="fas fa-paper-plane"></i>
      </button>
    </form>
  );
};

export default ChatInput;
