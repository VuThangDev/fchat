import React, { Component } from "react";
import "../user.css";
import SlideShowComponent from "./SlideShowComponent"

const collection = [
  { src: "/images/call1.jpg", title: "Làm việc hiệu quả hơn với Fchat call",caption: "Trao đổi công việc mọi lúc mọi nơi" },
  { src: "/images/chat1.png", title: "Giao tiếp dễ dàng hơn  với Fchat message" ,caption: "Giao tiếp mọi lúc, mọi nơi" },
  { src: "/images/search.jpg", title: "call effective" ,caption: "Tìm kiếm" },  
];

class SlideShow extends Component {

  render() {
    return (
      <div style={{ width:"100vw"}}>
        <SlideShowComponent 
          input={collection}
          ratio={`3:2`}
          mode={`automatic`}
          timeout={`3000`} />
      </div>
    );
  }
}

export default SlideShow;
