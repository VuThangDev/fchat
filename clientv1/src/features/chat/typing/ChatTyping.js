import React, { Component, useRef } from "react";
import "../user.css";
import CustomerApi from "../../../api/CustomerApi";
import { Link } from "react-router-dom";
import Axios from "axios";
import Message from "./Message";
import ChatInput from "./ChatInput";
import { HubConnectionBuilder } from "@microsoft/signalr";
import Infor from "./Infor";
import scrollIntoView from "react-scroll"

class ChatTyping extends Component {
  constructor() {
    super();
    this.state = { chat: [], messages: [], paramms: null };
    this.myref = React.createRef(this.state.chat);
    this.bottomRef = React.createRef();
  }

  scrollToBottom = () => {
    // this.bottomRef.current.scrollIntoView({ behavior: "smooth" });
  }


  componentDidMount(prevProps) {
    this.scrollToBottom();
    var iduser = this.props.match.params.idchat;
    var idfriend = this.props.match.params.idfriend;
   
    // if (this.props.match.params !== prevProps.match.params) {
      // this.fetchData(this.props.userID);
      fetch(
        `https://localhost:44350/api/Customer/GetMessageUser/${iduser}&&${idfriend}`
      )
        .then((res) => res.json())
        .then((data) => {
          this.setState({ messages: data });
          
        })
        .catch(console.log);
      const connection = new HubConnectionBuilder()
        .withUrl("https://localhost:44350/hubs/chat")
        .withAutomaticReconnect()
        .build();
  
      connection
        .start()
        .then((result) => {
          console.log("Connected!");
  
          connection.on("ReceiveMessage", (message) => {
            
            this.myref.current = this.state.chat;
            if (message.senderId == iduser || message.receiverId == iduser) {
              const updatedChat = [...this.myref.current];
              updatedChat.push(message);
  
              this.setState({ chat: updatedChat });
            }
          });
        })
        .catch((e) => console.log("Connection failed: ", e));
    // this.setState({ paramms: idfriend })
    
  }

  componentWillUnmount() {

  }

  renderMessageId() {
    var iduser = this.props.match.params.idchat;
    return this.state.messages.map((data, index) => {
      return data.senderId == iduser ? (
        <Message rendermessageId={iduser} datas={data.data} />
      ) : (
          <Message rendermessageId="-2" datas={data.data} />
        );
    });
  }
  renderChat() {
    var iduser = this.props.match.params.idchat;
    return this.state.chat.map((data, index) => {
      return data.senderId == iduser ? (
        <Message rendermessageId={iduser} datas={data.data} />
      ) : (
          <Message rendermessageId="-2" datas={data.data} />
        );
    }

    );
  }

  sendMessage = async (message) => {
    var iduser = Number(this.props.match.params.idchat);
    var idfriend = Number(this.props.match.params.idfriend);
    console.log(iduser)
    console.log(idfriend)
    const chatMessage = {
      Data: message,
      SenderId: iduser,
      ReceiverId: idfriend
    };
    try {
      await 
      ("https://localhost:44350/api/Chat/messages", {
        method: "POST",
        body: JSON.stringify(chatMessage),
        headers: {
          "Content-Type": "application/json",
          "Access-Control-Allow-Origin": "*",
          "Access-Control-Allow-Headers": "X-Requested-With",
        },
      }
      );
    } catch (e) {
      console.log("Sending message failed.", e);
    }
  };

  render() {
    var iduser = this.props.match.params.idchat;
    var idfriend = this.props.match.params.idfriend;
    return (
      <div id="chatInfor">
        <div id="chat">
          <div id="chat-header">
            <div className="chat-header-user">
              <div className="img-chat">
                <img
                  src="~/Public/anh/1.jpg"
                  alt=""
                  className="rounded-circle-chat"
                />
              </div>
              <div className="text-header">
                <h5 style={{ margin: "0px 0px 0px 40px" }}>RicardoT</h5>
                <small className="text-small">
                  <i style={{ margin: "0px 0px 0px 40px" }}>online</i>
                </small>
              </div>
            </div>
            <div className="chat-header-action">
              <ul className="list-inline">
                <li className="list-inline-item">
                  <i class="fas fa-info-circle"></i>
                </li>
              </ul>
            </div>
          </div>
          <div id="chat-content">
            <div id="messages">{this.renderMessageId()}
              {this.renderChat()}
              <div ref={this.messagesEndRef} />
            </div>

          </div>
          <div id="chat-footer">
            <div className="chat-f-head">
              <input type="file" id="input-file" name="file" />
              <label htmlFor="input-file">
                <i className="fas fa-image" />
              </label>
            </div>
            <ChatInput sendMessage={this.sendMessage} />
          </div>
        </div>
        <Infor userid={iduser} friendid={idfriend} />
      </div>
    );
  }
}

export default ChatTyping;
