import React, { useEffect, useState, useRef } from 'react';
import Message from "./Message";
import ChatInput from "./ChatInput";
import { HubConnectionBuilder } from "@microsoft/signalr";
import Infor from "./Infor";
import CustomerApi from "../../../api/CustomerApi";
import { scrollToBottom } from 'react-scroll/modules/mixins/animate-scroll';
import socket from '../../../connect';

var session = [];

const TypingChat = (props) => {
    var iduser = props.match.params.idchat;
    var idfriend = props.match.params.idfriend;


    const [state, setState] = useState({ messages: [] })
    const [info, setInfor] = useState([])
    const [chat, setChat] = useState([]);
    const latestChat = useRef(null);
    const [test, setTest] = useState([])
    const [idsocket, setIdocket] = useState('')
    const [userOnline, setUserOnline] = useState([])
    const [friend, setFriend] = useState({
        idcustomer: "",
        idfriend: "",
        nicknameCustomer: "",
        nicknamefriend: "",
    });
    const messagesEndRef = React.createRef()
    const [isSuccess, setIsSuccess] = useState(false);
    latestChat.current = chat;

    useEffect(() => {
        loadfriend();
        loadMessage()

        // CustomerApi.getInfor(
        //     iduser,
        //     idfriend
        // ).then(response => {
        //     console.log('res', response)
        //     setFriend({
        //         idcustomer: iduser,
        //         idfriend: idfriend,
        //         nicknameCustomer: response[0].nickNameCustomer,
        //         nicknamefriend: response[0].nickNameFriend,
        //     });

        // }).catch(err => {
        //     console.log(err)
        // });        
        //console.log(socket);

        // if (!session.includes(idfriend) || test[0] == null) {
        //     if (test[0] == null) {
        //         session = []
        //     }
        //     if (!session.includes(idfriend)) { session.push(idfriend); }
        //     setTest(idfriend)
        //     const connection = new HubConnectionBuilder()
        //         .withUrl("https://localhost:44350/hubs/chat")
        //         .withAutomaticReconnect()
        //         .build();

        //     connection
        //         .start()
        //         .then((result) => {
        //             console.log("Connected!");

        //             connection.on("ReceiveMessage", (message) => {

        //                 // Nếu nhận được mã ẩn thì không hiển thị ra, chỉ hiển thị là đã    
        //                 if (message.data == "%&$#H!D^!(!#!D!&*^E&!^") {
        //                     fetch(
        //                         `https://localhost:44350/api/Customer/isReader/${iduser}&&${idfriend}`
        //                     ).then((res) => res.json())
        //                         .then((data) => {

        //                         })
        //                         .catch(console.log);
        //                     setChat([])
        //                     loadMessage();
        //                 }
        //                 else {
        //                     if ((message.senderId == iduser && message.receiverId == idfriend) || (message.senderId == idfriend && message.receiverId == iduser)) {
        //                         const updatedChat = [...latestChat.current];
        //                         updatedChat.push(message);
        //                         setChat(updatedChat);

        //                     }
        //                 }


        //             });

        //         })
        //         .catch((e) => console.log("Connection failed: ", e));
        // }
        // else {
        // }

        socket.on("receiverMessage", (data) => {
            console.log('ho')
            const updatedChat = [...latestChat.current];
            updatedChat.push(data);
            setChat(updatedChat);
        });

        window.addEventListener('scroll', (event) => {
            sendMessage("%&$#H!D^!(!#!D!&*^E&!^", 1);
        })
        setChat([])
        setState({ messages: [] })

    }, [iduser, idfriend])


    const loadfriend = async () => {
        await CustomerApi.getInforFriend(
            iduser,
            idfriend
        ).then(response => {
            setInfor(response[0]);
            console.log(info)
            const local = JSON.parse(localStorage.getItem('userOnline'));
            const arr = [];
            arr.push(local);
            console.log(arr[0])
            var i = 0;
            for (i; i < arr[0].length; i++) {
                if ((arr[0][i].userId == response[0].FriendId) && (arr[0][i].socketId != null)) {
                    localStorage.setItem('userSocket', arr[0][i].socketId)
                    console.log('ids', localStorage.getItem('userSocket'));
                    break;
                }
                else {
                    setIdocket(['no']);
                }
            }
        }).catch(err => {
            console.log(err)
        });
    }

    function loadMessage() {
        fetch(
            `http://127.0.0.1:8000/api/getMessage/${iduser}&${idfriend}`
        )
            .then((res) => res.json())
            .then((data) => {
                console.log(data)
                setState({ ...state, messages: data });

            })
            .catch(console.log);
    }

    // const loadName = () => {
    //     return info.idcustomer == props.match.params.idchat ?
    //         info.nicknamefriend
    //         : info.nicknameCustomer
    // }

    // const loadNickName = () => {
    //     return info.idcustomer == props.match.params.idchat ?
    //         info.nicknameCustomer
    //         : info.nicknamefriend
    // }


    function renderMessageId() {
        var iduser = props.match.params.idchat;
        return state.messages.map((data, index) => {
            return data.SenderId == iduser ? (
                <Message rendermessageId={iduser} datas={data.Data} img={info.Pathavatar} isReader={data.Isreader} />
            ) : (
                <Message rendermessageId="-2" datas={data.Data} img={info.Pathavatar} />
            );
        });
    }
    function renderChat() {
        var iduser = props.match.params.idchat;
        return chat.map((data, index) => {
            return data.SenderId == iduser ? (
                <Message rendermessageId={iduser} datas={data.Data} img={info.Pathavatar} isReader={data.Isreader} />
            ) : (
                <Message rendermessageId="-2" datas={data.Data} img={info.Pathavatar} />
            );
        }

        );
    }

    // xu ly cuon cvhuot hoac click
    // Khi click thì sendMessage với mã ẩn lên sẻver



    const sendMessage = async (message, isReader) => {
        var today = new Date();
        var time = today.getHours() + ':' + today.getMinutes() + ':' + today.getSeconds()
        var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate()
        const chatMessage = {
            usertosend: localStorage.getItem('userSocket'),
            Data: message,
            SenderId: Number(iduser),
            ReceiverId: Number(idfriend),
            CreateAt: date,
            SendingTime: time,
            Isreader: isReader
        };

        await socket.emit("sendMessage", chatMessage);
        const updatedChat = [...latestChat.current];
        updatedChat.push(chatMessage);
        setChat(updatedChat);
    };

    function callVideo() {

        window.open('http://localhost:3000/video', 'sharer', 'toolbar=0,status=0,width=1280,height=719');
    }
    return (
        <div id="chatInfor">
            <div id="chat">
                <div id="chat-header">
                    <div className="chat-header-user">
                        <div className="img-chat">
                            <img
                                src={info.Pathavatar}
                                alt=""
                                className="rounded-circle-chat"
                            />
                        </div>
                        <div className="text-header">
                            <h5 style={{ margin: "0px 0px 0px 40px" }}>{info.NickNameFriend}</h5>
                            <small className="text-small">
                                <i style={{ margin: "0px 0px 0px 40px" }}>online</i>
                            </small>
                        </div>
                    </div>
                    <div className="chat-header-action">
                        <ul className="list-inline">
                            <li className="list-inline-item" onClick={() => callVideo()}>
                                <i class="fas fa-video"></i>
                            </li>
                        </ul>
                    </div>
                </div>
                <div id="chat-content">
                    <div id="messages">

                        {renderMessageId()}
                        {renderChat()}

                    </div>

                </div>
                <div id="chat-footer">
                    <div className="chat-f-head">
                        <input type="file" id="input-file" name="file" />
                        <label htmlFor="input-file">
                            <i className="fas fa-image" />
                        </label>
                    </div>
                    <ChatInput sendMessage={sendMessage} clickSeen={sendMessage} />
                </div>
            </div>
            <Infor friendProps={info} />
        </div>
    );
}

export default TypingChat;