import React, { useState, useEffect } from "react";
import "../user.css";
import CustomerApi from "../../../api/CustomerApi";

const AddUser = (props)=>{

  var iduser = props.match.params.id;
  const [helpgetfriend, setHelpgetfriend] = useState([])

  useEffect(()=>{
    CustomerApi.helpGetFriend(
      iduser      
    ).then(response => {          
      setHelpgetfriend(response)      
    }).catch(err => {
      console.log(err)
    });

  },[])

  
  function LoadHelpFriend(){
    
    return helpgetfriend.map((data, index) => {
      return(
        <a href="#" className="icon-user-a col-3 col-s-4">
        <div className="icon-user">
          <div>
            <img src={data.pathavatar} alt=""  className="imgUserHelp"/>
          </div>
          <div className="info">
          <div className="nameUserHelp">{data.name}</div>
          <div >Người bạn có thể biết</div>
          </div>
          <button>Kết bạn</button>
          <i className="fas fa-times" title="Xoá gợi ý kết b" />
        </div>
      </a>)
    }
    );      
  }

    return (
      <div id="add-friend">
        <header>
          <div>
            <div id="add-head">
              <div className="img-add-head">
                <i className="fas fa-user-plus" />
              </div>
              <h3>Danh sách kết bạn</h3>
            </div>
          </div>
        </header>
        <div className="content-add">
          <div id="content-head">
            <div id="content-head-left">
              Gợi ý kết bạn (<span>5</span>)
            </div>
            <div id="content-head-right">Xem thêm &gt;</div>
          </div>
          <div id="content-bottom">
            {LoadHelpFriend()}
          </div>
        </div>
      </div>
    );
  
}

export default AddUser;
