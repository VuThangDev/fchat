import React, {Component} from 'react';
import ReactDOM from 'react-dom';


export default class SlideShowComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      slideIndex: 0
    };
    const ratioWHArray = this.props.ratio.split(":");
    this.ratioWH = ratioWHArray[0] / ratioWHArray[1];

    this.backward = this.backward.bind(this);
    this.forward = this.forward.bind(this);
    this.setSlideIndex = this.setSlideIndex.bind(this);
    this.getNewSlideIndex = this.getNewSlideIndex.bind(this);
    // this.updateDimensions = this.updateDimensions.bind(this);
    this.runAutomatic = this.runAutomatic.bind(this);
  }
  getNewSlideIndex(step) {
    const slideIndex = this.state.slideIndex;
    const numberSlide = this.props.input.length;

    let newSlideIndex = slideIndex + step;

    if (newSlideIndex >= numberSlide) newSlideIndex = 0;
    else if (newSlideIndex < 0) newSlideIndex = numberSlide - 1;
    return newSlideIndex;
  }

  backward() {
    console.log("tests")
    this.setState({
      slideIndex: this.getNewSlideIndex(-1)
    });
  }


  forward() {
    this.setState({
      slideIndex: this.getNewSlideIndex(1)
    });
  }


  setSlideIndex(index) {
    this.setState({
      slideIndex: index
    })
  }

  // updateDimensions() {
  //   this.containerElm.style.height 
  //     = `${this.containerElm.offsetWidth / this.ratioWH}px`;
  // }

  runAutomatic() {
    this.setState({
      slideIndex: this.getNewSlideIndex(1)
    });
  }
  componentDidMount() {
    this.rootElm = ReactDOM.findDOMNode(this);
    this.containerElm = this.rootElm.querySelector(".container");

    // this.updateDimensions();
    // window.addEventListener("resize", this.updateDimensions);
    
    if (this.props.mode === "automatic") {
      const timeout = this.props.timeout || 5000;

      this.automaticInterval = setInterval(
        () => this.runAutomatic(),
        Number.parseInt(timeout)
      );
    }
  }
  componentWillUnmount() {
    window.removeEventListener("resize", this.updateDimensions);
    if (this.automaticInterval) clearInterval(this.automaticInterval);
  }
  render() {
    return (
      <div className="lp-slideshow">        
        <div className="container">
          {
            this.props.input.map((image, index) => {
              return (
                <div key={index}
                className={
                  `slide ${this.state.slideIndex === index ? "active" : ""}`
                } style={{height:"100vh"}}>
            <div id="slide-show" style={{margin:"0px auto"} }>            
            <div
              style={{
                width: "415px",
                textAlign: "center",
                color: "rgb(34, 34, 34)",
                marginBottom: "50px",
                
              }}
            >
              <div style={{ fontSize: "20px" }}>
                Chào mừng bạn đến với{" "}
                <span style={{ fontSize: "24px" }}>Fchat</span>
              </div>
              <span style={{ fontSize: "14px" }}>
                Khám phá những tiện ích hỗ trợ làm việc và trò chuyện cùng người
                thân, bạn bè được tối ưu hoá cho máy tính của bạn.
              </span>
              </div>
                <div className="show-page-text">
                  <img src={image.src} alt="" className="image-chat" />
                  <span className="show-page-text-title truct2">
                    {image.title}
                  </span>
                  <span className="show-page-text-subtitle truct5">
                    {image.caption}
                  </span>
                </div>        
              </div>              
            </div>
                  )
                })
              }
            <span
              className="size"              
              onclick={this.backward}
            >❮</span>
            <span className="size next" onclick={this.forward}>❯</span>
        </div>            
            <div className="slide-show-button">
              {
                this.props.input.map((_, index) => {
                  return (
                    <span
                      key={index}
                      className={
                        `dot ${this.state.slideIndex === index ? "active" : ""}`
                      }
                      onClick={() => this.setSlideIndex(index)}
                    >
                    </span>
                  )
                })
              }
            </div>
      </div>

    );
  }
}