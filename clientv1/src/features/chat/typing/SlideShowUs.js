import React, { Component } from "react";
import "../user.css";

class SlideShowUs extends Component {
  render() {
    return (
      <div id="slide-show-us">
        <i
          className="fas fa-angle-left size"
          style={{ left: "415px" }}
          onclick="plusDivs1(-1)"
        />
        <div className="show-page-text-fr">
          <span
            className="show-page-text-title truct2"
            style={{ marginBottom: "20px" }}
          >
            Thật vui khi có nhiều bạn bè
          </span>
          <img src="~/Public/anh/friend.jpg" alt="" className="image-chat" />
        </div>
        <div className="show-page-text-fr">
          <span
            className="show-page-text-title truct2"
            style={{ marginBottom: "20px" }}
          >
            Hãy cùng kết nối với những người bạn nào
          </span>
          <img src="~/Public/anh/friend1.jpg" alt="" className="image-chat" />
        </div>
        <div className="slide-show-button">
          <span className="btn2 " onclick="currentDiv1(1)" />
          <span className="btn2 " onclick="currentDiv1(2)" />
        </div>
        <i className="fas fa-angle-right size next " onclick="plusDivs1(1)" />
      </div>
    );
  }
}

export default SlideShowUs;
