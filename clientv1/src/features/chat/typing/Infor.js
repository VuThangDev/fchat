import React, { useEffect, useState } from "react";

import Popup from "reactjs-popup";
import Modal from "react-modal"
import Axios from 'axios';
const Infor = (props) => {

  console.log(props.friendProps);
  const [friend, setFriend] = useState({
    UserId: '',
    FriendId: '',
    nicknameCustomer: '',
    nicknamefriend: '',
  });
  
  useEffect(() => {
    
    console.log('render!')
   
    console.log("code infor",props.friendProps)
     
    setFriend({
      UserId: Number(props.friendProps.UserId),
      FriendId: Number(props.friendProps.FriendId),
      nicknameCustomer: props.friendProps.NickNameCustomer,
      nicknamefriend: props.friendProps.NickNameFriend,
  });
      
    
  }, [friend.nicknamefriend]);

  function updateNickName(){
    console.log('aaa',friend)
    Axios.post("http://127.0.0.1:8000/api/updatenickname", { ...friend }).then(
      res => res.data
    ).then(res => {
      console.log(res)
      if (res.Status == "Invalid") alert("Invalid User");
      else {
        alert("thanh cong")
      };
    });
  }
  const handleChangeuser = e => {
    setFriend({ ...friend, nicknameCustomer: e.target.value });
  }
  const handleChangefriend = e => {
    setFriend({ ...friend, nicknamefriend: e.target.value });
  }
  
    
  return (
    <div id="sidebar-group-chat" className="sidebar-group-1">
      <header>
        <span>Thông tin</span>
      </header>
      <div id="more">
        <div id="imgavatar">
          <div className="show-page-text-more">
            <img src={props.friendProps.Pathavatar} alt="" className="image-more-chat" />
            <span className="show-page-text-title truct2">{friend.nicknamefriend}</span>
          </div>
        </div>
        <div className="_1lizz _1liy">
          <div style={{ borderTop: "1px solid #E1DFDF" }} />
          <h4 className="_1lizmain _1lizp">
            HÀNH ĐỘNG KHÁC
            <div style={{ float: "left", fontSize: "25px" }}>
              <i className="fas fa-angle-down d" />
              <i className="fas fa-angle-left l" style={{ display: "none" }} />
            </div>
          </h4>
          <div className="sp">
            <span>
              <Popup modal trigger={
              <div className="_3szo" id="find-click">
                <div>Tìm kiếm trong cuộc trò chuyện</div>
                <div style={{ marginLeft: "85px" }}>
                  <i className="fas fa-search" />
                </div>
              </div>}>
                {close =>(
                  <div id="find-user">
                  <div className="icon-find" style={{display: 'flex', alignItems: 'center'}}>
                    <div className="ic-f"><i className="fas fa-angle-up" /></div>
                    <div className="ic-f"><i className="fas fa-angle-down" /></div>
                  </div>
                  <div> <input type="text" id="f-us" placeholder="Nhập tên cần tìm kiếm" style={{width: '450px', height: '29px'}} /></div>
                  <div className="back" style={{marginLeft: '200px', color: 'blue', fontSize: '22px', cursor: 'pointer'}} onClick={()=>close()}>Đóng</div>
                  <div className="find" style={{marginLeft: '200px', color: 'blue', fontSize: '22px', cursor: 'pointer', display: 'none'}}>Tìm kiếm</div>
                </div>
                )}
              </Popup>
              
              <div className="_3szo" id="edit-click">
                <Popup modal
            nested trigger={<div>Chỉnh sửa biệt danh</div>}>
              {close =>(
                  <div id="edit-name">
                    <h2>
                      <span>
                        <button
                          style={{
                            border: "none",
                            fontSize: "21px",
                            color: "blue",
                            padding: "0px 16px",
                            height: "50px",
                          }}
                          onClick={()=>close()}
                        >
                          Huỷ
                        </button>
                      </span>
                      <div>Chỉnh sửa biệt danh</div>
                    </h2>
                    <Popup position="top center" closeOnDocumentClick
                       trigger={<div className="edit-name-right">
                          <div>
                            <a href="#" style={{ textDecoration: "none" }}>
                              <div className="img-edit">
                                <div className="img-edit-in">
                                  <img
                                    src="anh/1.jpg"
                                    alt=""
                                    className="img-ed"
                                  />
                                </div>
                              </div>
                              <div className="name-edit">
                                <div>{props.friendProps.NickNameCustomer}</div>
                              </div>
                            </a>
                          </div>
                        </div>}>
                        
                          {close=>(<div id="editer">
                        <div id="editer-out" />
                        <div>
                          <div className="edit-us-in">
                            <h2
                              style={{
                                margin: "0px",
                                marginBottom: "6px",
                                fontSize: "18px",
                                fontWeight: 500,
                              }}
                            >
                              Chỉnh sửa biệt danh của {props.friendProps.NickNameCustomer}
                            </h2>
                            <div
                              style={{
                                fontSize: "15px",
                                fontWeight: 400,
                                marginBottom: "24px",
                              }}
                            >
                              <div style={{ paddingBottom: "10px" }}>
                                Mọi người trong cuộc hội thoại sẽ thấy biệt danh
                                này
                              </div>
                              <div className="ip-edit">
                                <input
                                  style={{
                                    width: "325px",
                                    height: "35px",
                                    border: "1px solid rgba(0, 0, 0, .10)",
                                    cursor: "text",
                                    padding: "10px 8px 6px",
                                  }}                                  
                                  onChange={handleChangeuser}
                                  
                                  placeholder={props.friendProps.NickNameCustomer}
                                />
                                <div title="Gỡ" style={{ marginLeft: "5px" }}>
                                  <i className="fas fa-times" />
                                </div>
                              </div>
                            </div>
                            <div className="btn-edit">
                              <div style={{ marginLeft: "206px" }}>
                                <span>
                                  <button className="close" onClick={()=> close()}>Huỷ</button>
                                </span>
                                <span
                                  style={{
                                    border: "1px solid #B5B5B5",
                                    width: "1px",
                                    height: "10px",
                                  }}
                                />
                                <span>
                                  <button style={{ fontWeight: 600 }}>
                                    Lưu
                                  </button>
                                </span>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>)}               
                      
                      </Popup>
                    <Popup position="top center" trigger={<div className="edit-name-right">
                      <div>
                        <a href="#" style={{ textDecoration: "none" }}>
                          <div className="img-edit">
                            <div className="img-edit-in">
                              <img src="anh/2.jpg" alt="" className="img-ed" />
                            </div>
                          </div>
                          <div className="name-edit">
                            <div>{props.friendProps.NickNameFriend}</div>
                          </div>
                        </a>
                      </div>
                    </div>}>
                      {close =>( <div id="editer">
                        <div id="editer-out" />
                        <div>
                          <div className="edit-us-in">
                            <h2
                              style={{
                                margin: "0px",
                                marginBottom: "6px",
                                fontSize: "18px",
                                fontWeight: 500,
                              }}
                            >
                              Chỉnh sửa biệt danh của {props.friendProps.NickNameFriend}
                            </h2>
                            <div
                              style={{
                                fontSize: "15px",
                                fontWeight: 400,
                                marginBottom: "24px",
                              }}
                            >
                              <div style={{ paddingBottom: "10px" }}>
                                Mọi người trong cuộc hội thoại sẽ thấy biệt danh
                                này
                              </div>
                              <div className="ip-edit">
                                <input
                                  style={{
                                    width: "325px",
                                    height: "35px",
                                    border: "1px solid rgba(0, 0, 0, .10)",
                                    cursor: "text",
                                    padding: "10px 8px 6px",
                                    
                                  }                                  
                                }
                                  onChange={handleChangefriend}
                                  placeholder={props.friendProps.NickNameFriend}
                                />
                                <div title="Gỡ" style={{ marginLeft: "5px" }}>
                                  <i className="fas fa-times" />
                                </div>
                              </div>
                            </div>
                            <div className="btn-edit">
                              <div style={{ marginLeft: "206px" }}>
                                <span>
                                  <button className="close" onClick={()=> close()}>Huỷ</button>
                                </span>
                                <span
                                  style={{
                                    border: "1px solid #B5B5B5",
                                    width: "1px",
                                    height: "10px",
                                  }}
                                />
                                <span>
                                  <button style={{ fontWeight: 600 }} onClick={updateNickName}>
                                    Lưu
                                  </button>
                                </span>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>)}
                     
                    </Popup>

                  </div>)}
                </Popup>
                <div style={{ marginLeft: "155px" }}>
                  <i className="fas fa-pencil-alt" />
                </div>
              </div>
            </span>
          </div>
        </div>
        <div className="_1lizz _1liy">
          <div style={{ borderTop: "1px solid #E1DFDF" }} />
          <h4 className="_1lizmain _1lizp">
            quyền riêng tư
            <div style={{ float: "left", fontSize: "25px" }}>
              <i className="fas fa-angle-down d" />
              <i className="fas fa-angle-left l" style={{ display: "none" }} />
            </div>
          </h4>
          <div className="sp">
            <span>
              <div className="_3szo">
                <div>Chặn tin nhắn</div>
                <div style={{ marginLeft: "194px" }}>
                  <i className="fas fa-ban" />
                </div>
              </div>
              <div className="_3szo">
                <div>Góp ý và báo cáo cuộc trò chuyện</div>
                <div style={{ marginLeft: "67px" }}>
                  <i className="fas fa-exclamation-triangle" />
                </div>
              </div>
            </span>
          </div>
        </div>
        <div className="_1lizz _1liy">
          <div style={{ borderTop: "1px solid #E1DFDF" }} />
          <h4 className="_1lizmain _1lizp">
            Ảnh đã chia sẻ
            <div style={{ float: "left", fontSize: "25px" }}>
              <i className="fas fa-angle-down d" />
              <i className="fas fa-angle-left l" style={{ display: "none" }} />
            </div>
          </h4>
          <div className="sp img-info-friend _3szo">
            <a href="#" className="col-4">
              <div className="_3m31">
                <img src="/images/9.jpg" alt="" />
              </div>
            </a>
            <a href="#" className="col-4">
              <div className="_3m31">
                <img src="/images/10.jpeg" alt="" />
              </div>
            </a>
            <a href="#" className="col-4">
              <div className="_3m31">
                <img src="/images/6.jpg" alt="" />
              </div>
            </a>
            <a href="#" className="col-4">
              <div className="_3m31">
                <img src="/images/quadz.jpg" alt="" />
              </div>
            </a>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Infor;
