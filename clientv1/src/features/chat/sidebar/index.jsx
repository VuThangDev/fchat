import React, { useEffect, lazy, Suspense } from "react";
import {
  BrowserRouter,
  Redirect,
  Route,
  Router,
  Switch,
  useRouteMatch,
} from "react-router-dom";

const typing = React.lazy(() => import("../typing"));

function Sidebardefault() {
  const match = useRouteMatch();

  return (
    <Suspense fallback={<div>loading ...</div>}>
      <BrowserRouter>
        <Switch>
          {/* <Route path="/sidebar/chat" component={SlideShow} />     */}
        </Switch>
      </BrowserRouter>
    </Suspense>
  );
}

export default Sidebardefault;
