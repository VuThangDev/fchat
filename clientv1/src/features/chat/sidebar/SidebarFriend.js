import React, { useState, useEffect, location, useCallback } from "react";
import "../user.css";
import CustomerApi from "../../../api/CustomerApi";
import { Redirect } from "react-router-dom";
import { Link } from "react-scroll";
import axiosClient from 'axios'

const SidebarFriend = (props) => {

  var iduser = props.match.params.id;

  const [invatations, setInvatation] = useState([])
  const [isSuccess, setIsSuccess] = useState(false);

  useEffect(() => {
    getFriends()
  }, [])

  const getFriends = useCallback(() => {
    CustomerApi.getFriendAdd(
      iduser
    ).then(response => {
      setInvatation(response)
      console.log(invatations)
    }).catch(err => {
      console.log(err)
    });
  }, [])

  // useEffect(() => {
  //   if (isSuccess) {
  //     getFriends()
  //   }
  // }, [isSuccess])


  function ClickAdd(id) {
    var params = { idUser: Number(iduser), idFriend: id, Status: "accept" }
    CustomerApi.acceptFriend(params)
      .then(success => {
        if (success) {
          getFriends();
          setIsSuccess(true);
        } else {
          setIsSuccess(false);
        }
      });
    // iduser= "";
  }

  function ClickCancel(id) {
    var params = { idUser: Number(iduser), idFriend: id, Status: "cancel" }
    CustomerApi.acceptFriend(params)
      .then(success => {
        if (success) {
          setIsSuccess(true);
        } else {
          setIsSuccess(false);
        }
      });
  }

  function LoadFriendAdd() {
    console.log("in", invatations)
    return invatations.map((data, index) => {
      return (
        <div className="list-group-item-wait" key={index}>
          <div className="~/Public/anh">
            <img
              src={data.pathavatar}
              alt="anhdd"
              className="rounded-circle"
            />
          </div>
          <div className="users-list-body-wait">
            <h5 className="NameAdd">{data.name}</h5>
            <div className="outbtnAdd">
              <div className="btnAdd">
                <button className="btnClick btnConfirm" onClick={() => ClickAdd(data.id)}>Xác nhận</button>
                <button className="btnClick" onClick={() => ClickCancel(data.id)}>Xóa</button>
              </div>
            </div>
          </div>
        </div>)
    }
    );
  }

  return (
    <div id="friend">
      <header className="head">
        <span>Friends</span>
        <form action="#">
          <input
            type="text"
            name
            id
            className="form-control"
            
            placeholder="Nhập bạn cần tìm"
          />
        </form>
      </header>
      <div
        className="sidebar-body-fr"
        ng-app="MyApp"
        ng-controller="Mycontroller"
      >
        <ul className="list-group list-group-flush" ng-repeat="s in datas">        
        </ul>
      </div>
      <div>
        <div id="wait">
          <div style={{ borderTop: "1px solid #E1DFDF" }} />
          <h4 className="_1lizmain _1lizp _message-wait">
            Lời mời kết bạn
              <div style={{ float: "left", fontSize: "25px" }}>
              <i className="fas fa-angle-down d" />
              <i
                className="fas fa-angle-left l"
                style={{ display: "none" }}
              />
            </div>
          </h4>
          {LoadFriendAdd()}
        </div>
      </div>
    </div>
  );

}

export default SidebarFriend;
