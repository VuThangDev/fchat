import React, { Component } from "react";
import Axios from "axios";
import CustomerApi from "../../../api/CustomerApi";
import { stringify, queryString } from "query-string";
import "../user.css";
import { Link } from "react-router-dom";
import { withRouter } from "react-router";
import axios from "axios";


// import queryString from 'query-string';

class SidebarChat extends Component {
  constructor(props) {
    super(props);
    this.state = { users: [], storageId: localStorage.getItem('@currentUser'), name: "" };
    this.NameFind = this.NameFind.bind(this);
  }

  componentDidMount() {
    var iduser = this.props.match.params.idchat;
    this.function1();
  }

  async function1(){
    await fetch(`https://localhost:44350/api/Customer/GetUser/${this.state.storageId}`)
    .then((res) => res.json())
    .then((data) => {
      this.setState({ users: data });
      console.log(this.state.users)
    })
    .catch(console.log);
  }

  NameFind(event) {
    this.setState({ name: event.target.value });
  }

  findFriend() {
    // var params = { Name: this.state.name, UserId: this.state.storageId }
    
    axios.post("https://localhost:44350/api/Customer/FindFriend", { Name: this.state.name, UserId: this.state.storageId })
      .then(res => res.json())
      .then(res => { this.setState({ users: res }); console.log("friend", this.state.users) })
  }

  renderUsers() {

    var iduser = this.props.match.params.idchat;
    return this.state.users.map((data, index) => {
      return (
        <Link to={`/c/${iduser}/chat/message/${data.id}`}>
          <li className="list-group-item">
            <div
              className="anh"
              onClick={() => {
                console.log("aaa");
              }}
            >
              <img class="rounded-circle" src={data.pathavatar} />
            </div>
            <div className="users-list-body">
              <h5>{data.name}</h5>
              <p></p>
            </div>
            <div className="users-list-action"></div>
          </li>
        </Link>)
    }

    );

  }

  render() {
    return (
      <div id="chats" className="sidebar active">
        <header className="head">
          <span>Chats</span>

          <input
            type="text"
            name
            id="f-us"
            className="form-control"
            placeholder="Search friend"
            value={this.state.NameFind}
            onChange={this.NameFind}
          />

          <div onClick={this.findFriend}>Find</div>

        </header>
        <div className="sidebar-body virtualized-scroll">
          <ul id="user-sidebar" className="list-group list-group-flush">
            {this.renderUsers()}
          </ul>
        </div>
      </div>
    );
  }
}

export default withRouter(SidebarChat);
