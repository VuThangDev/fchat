import React, { useEffect, useState } from 'react';
import Axios from "axios";
import CustomerApi from "../../../api/CustomerApi";
import { stringify, queryString } from "query-string";
import "../user.css";
import { Link } from "react-router-dom";
import { withRouter } from "react-router";
import axios from "axios";


const SidebarChat = (props) => {

    const [state, setState] = useState({ users: [], storageId: localStorage.getItem('@currentUser'), searchText: "" });

    useEffect(() => {
        getFirend()
    }, [])
    var iduser = props.match.params.idchat;
    async function getFirend() {
        await fetch(`http://127.0.0.1:8000/api/getfriend/${iduser}`)
            .then((res) => res.json())
            .then((data) => {    
                console.log(data)                            
                setState({ users: data });
            })
            .catch(console.log);
    }

    function handleChangeSearchText(e) {
        setState({ ...state, searchText: e.target.value })
    }

    function handleFindFriend() {
        axios.post("http://127.0.0.1:8000/api/findfriend", { Name: state.searchText, UserId: 1 })
            .then(res => {
                setState({ users: res.data });
                console.log( 'dsadasdasdas',state.users)
            })        
    }

    function renderUsers() {
        var iduser = props.match.params.idchat;
        return state.users && state.users.map((data, index) => {
            return (
                <Link to={`/c/${iduser}/chat/message/${data.FriendId}`}>
                    <li className="list-group-item">
                        <div
                            className="anh"
                            onClick={() => {
                                console.log("aaa");
                            }}
                        >
                            <img class="rounded-circle" src={data.Pathavatar} />
                        </div>
                        <div className="users-list-body">
                            <h5 className="NameFriendChat">{data.Name}</h5>
                            <p></p>
                        </div>
                        <div className="users-list-action"></div>
                    </li>
                </Link>
            )
        }
        );

    }

    return (
        <div id="chats" className="sidebar active">
            <header className="head">
                <span>Chats</span>
                <div style={{ display: "flex" }}>
                    <input
                        type="text"
                        name
                        id="f-us"
                        className="form-control"
                        placeholder="Search friend"
                        value={state.searchText}
                        onChange={handleChangeSearchText}
                    />

                    <div onClick={handleFindFriend} className="FindFriendChat"><i class="faSendFind far fa-paper-plane"></i></div>
                </div>
            </header>
            <div className="sidebar-body virtualized-scroll">
                <ul id="user-sidebar" className="list-group list-group-flush">
                    {renderUsers()}
                </ul>
            </div>
        </div>
    )
}

export default SidebarChat;