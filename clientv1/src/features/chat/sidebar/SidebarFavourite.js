import React, { Component } from "react";
import "../user.css";

class SidebarFavourite extends Component {
  render() {
    return (
      <div id="favourite">
        <header className="head">
          <span>favourite</span>
          <form action="#">
            <input
              type="text"
              name
              id
              className="form-control"
              placeholder="Search chat"
            />
          </form>
        </header>
        <div className="sidebar-body virtualized-scroll">
          <ul className="list-group list-group-flush" onclick="clickuser()">
            <li className="list-group-item">
              <div className="~/Public/anh">
                <img
                  src="~/Public/anh/1.jpg"
                  alt=""
                  className="rounded-circle"
                />
              </div>
              <div className="users-list-body">
                <h5>RicardoT</h5>
                <p>một hai ba bốn</p>
              </div>
              <div className="users-list-action">
                <div className="new-message-count">2</div>
              </div>
            </li>
            <li className="list-group-item">
              <div className="~/Public/anh">
                <img
                  src="~/Public/anh/2.jpg"
                  alt=""
                  className="rounded-circle"
                />
              </div>
              <div className="users-list-body">
                <h5>RicardoT</h5>
                <p>một hai ba bốn</p>
              </div>
              <div className="users-list-action">
                <div className="new-message-count">2</div>
              </div>
            </li>
            <li className="list-group-item open-chat">
              <div className="~/Public/anh">
                <img
                  src="~/Public/anh/3.jpg"
                  alt=""
                  className="rounded-circle"
                />
              </div>
              <div className="users-list-body">
                <h5>RicardoT</h5>
                <p>một hai ba bốn</p>
                <div></div>
              </div>
              <div className="users-list-action">
                <div className="new-message-count">2</div>
              </div>
            </li>
            <li className="list-group-item">
              <div className="~/Public/anh">
                <img
                  src="~/Public/anh/4.jpg"
                  alt=""
                  className="rounded-circle"
                />
              </div>
              <div className="users-list-body">
                <h5>RicardoT</h5>
                <p>một hai ba bốn</p>
              </div>
              <div className="users-list-action">
                <div className="new-message-count">2</div>
              </div>
            </li>
            <li className="list-group-item">
              <div className="~/Public/anh">
                <img
                  src="~/Public/anh/5.jpg"
                  alt=""
                  className="rounded-circle"
                />
              </div>
              <div className="users-list-body">
                <h5>RicardoT</h5>
                <p>một hai ba bốn</p>
              </div>
              <div className="users-list-action">
                <div className="new-message-count">2</div>
              </div>
            </li>
          </ul>
        </div>
      </div>
    );
  }
}

export default SidebarFavourite;
