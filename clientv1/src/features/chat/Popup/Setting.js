import React from 'react';

const SettingPopup=(props)=>{
    return(
        <div id="smenu">
        <div id="smenu-in">
          <div className="smenu-item introduce-click">
            Giới thiệu
          </div>
          <div className="smenu-line" />
          <div className="smenu-item" style={{color: 'red'}}>
            Đăng xuất 
          </div>
          <div className="smenu-item" onClick={()=>props.closePopup}>
            Thoát
          </div>
        </div>
      </div>
    )
}

export default SettingPopup;