import React, { useEffect, lazy, Suspense, useState, createContext } from "react";
import {
  BrowserRouter,
  Link,
  Redirect,
  Route,
  Router,
  Switch,
  useParams,
  useRouteMatch,
} from "react-router-dom";
import SidebarFavourite from "./sidebar/SidebarFavourite";
import SidebarFriend from "./sidebar/SidebarFriend";
import SlideShow from "./typing/SlideShow";
import "./user.css";
import CustomerApi from "../../api/CustomerApi";
import SettingPopup from "./Popup/Setting";
import Popup from "reactjs-popup";
import Axios from 'axios';
import { io } from 'socket.io-client';
import { Children } from "react";
import { Howl } from "howler";
import ringtone from '../../Sounds/ringtone.mp3'
import socket from '../../connect';
const sidebar = React.lazy(() => import("./sidebar"));
localStorage.setItem('caller', 'caller');



const ringtoneSound = new Howl({
  src: [ringtone],
  loop: true,
  preload: true
})



function Chatdefault(props) {
  // const match = useRouteMatch();

  let { idchat } = useParams();
  const [state, setState] = useState({ address: '', birthday: '', Id: Number(idchat) })
  const [user, setUser] = useState([])
  const [open, setOpen] = useState(false);
  const [newUser, setNewUser] = useState({})
  const [userJoin, setUserJoin] = useState(false);
  const [receivingCall, setReceivingCall] = useState(false);
  const [caller, setCaller] = useState("");
  const [callerSignal, setCallerSignal] = useState({});

  useEffect(() => {
    localStorage.setItem('acceptedCall', 'ring');
    socket.on('me', id => localStorage.setItem('socketId', id))
    console.log('id', localStorage.getItem('socketId'))
    async function fetchUser() {
      try {
        await fetch(`http://127.0.0.1:8000/api/Getuser/${idchat}`)
          .then((res) => res.json())
          .then((data) => {
            console.log(data)
            setUser(data[0]);
            console.log(user)
          })
          .catch(console.log);
      } catch (error) {
        console.log(error);
      }
    }
    socket.emit('sendUser', { userId: idchat, socketId: localStorage.getItem('socketId') })
    fetchUser();

    socket.on('DANH_SACH_ONLINE', user => {
      localStorage.setItem('userOnline', JSON.stringify(user))     
    })


    socket.on('callUser', (data) => {
      localStorage.setItem('caller', 'answer');
      console.log(data)
      console.log(data.from)
      ringtoneSound.play();
      setReceivingCall(true);
      localStorage.setItem('callers', data.from);
    });


  }, [])

  const handleClose = () => {
    setOpen(false);
  }
  const handleOpen = () => {
    setOpen(true);
  }

  const handleChangeAddress = e => {
    setState({ ...state, address: e.target.value });
  }
  const handleChangeBirthday = e => {
    setState({ ...state, birthday: e.target.value });
  }

  const handlleUpdate = () => {
    Axios.post("http://127.0.0.1:8000/api/updateuser", { ...state }).then(
      res => res.data
    ).then(res => {
      if (res.Status == "Invalid") alert("Invalid User");
      else {
        alert("thanh cong")
      };
    });
  }
  function acceptCall() {
    ringtoneSound.unload();
    localStorage.setItem('answerCall', 'accept');
    setReceivingCall(false);
    window.open('http://localhost:3000/video', 'sharer', 'toolbar=0,status=0,width=1280,height=719');
  }

  function rejectCall() {
    ringtoneSound.unload();
    setReceivingCall(false);
  }
  let incomingCall;
  if (receivingCall) {
    incomingCall = (
      <div className="incomingCallContainer">
        <div id="notification_out"></div>
        <div className="incomingCall flex flex-column">
          <div><span className="callerID"></span> Trả lời em êi</div>
          <div className="incomingCallButtons">
            <button name="accept" className="btncallUser btnaccept" onClick={() => acceptCall()}><i class="fa-video-call fas fa-video"></i>  Chấp nhận</button>
            <button name="reject" className="btncallUser" onClick={() => rejectCall()}>Từ Chối</button>
          </div>
        </div>
      </div>
    )
  }
  return (
    <div>
      {incomingCall}
      <div id="layout">
        <div id="navigation">
          <div className="nav-group">
            <div className="nav-group">
              <Popup modal nested
                trigger={<div className="menuimg" >
                  <img src={user.Pathavatar} alt="test" style={{ width: "50px", height: "50px", borderRadius: "50%", margin: "9px 0px 0px 9px" }} />
                </div>}>
                {close => (<div id="editProfile">
                  <div className="editProfile-in">
                    <header>
                      <div style={{ fontSize: '17px', fontWeight: 600, display: 'flex' }}>
                        <span>Cập nhật thông tin</span>
                        <i className="fas fa-times" style={{ marginLeft: '165px' }} onClick={close} />
                      </div>
                    </header>
                    <div>
                      <img src="/images/hoadao.jpg" alt="ảnh bìa" className="coverImage" />
                      <div id="EditContent">
                        <div id="EditContentMain">
                          <div id="NameImages">
                            <img src={user.Pathavatar} alt="ảnh đại diện" className="avatarImage" />
                            <div id="NameEditProfile">
                              <span id="NameEdit">{user.Name}</span>
                              <i className="far fa-edit" />
                            </div>
                          </div>
                          <div>
                            <label className="labelEdit">Quê quán</label>
                            <input placeholder={user.Address} className="inputEdit" onChange={handleChangeAddress} value={state.address} />
                          </div>
                          <div>
                            <label className="labelEdit">Ngày sinh</label>
                            <input type="date" className="inputEdit" onChange={handleChangeBirthday} value={state.birthday} />
                          </div>
                          <div>
                            <label className="labelEdit">Giới tính</label>
                            <div style={{ display: 'flex' }}>
                              <div>
                                <input type="radio" defaultChecked="false" name="gioitinh" defaultValue="nam" />
                                <label htmlFor>Nam</label>
                              </div>
                              <div style={{ marginLeft: '20px' }}>
                                <input type="radio" name="gioitinh" defaultValue="nu" />
                                <label htmlFor>Nữ</label>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div id="FooterContent">
                        <button className="z__btn" style={{ marginLeft: '150px' }}>Huỷ</button>
                        <button className="z__btn Z__update" onClick={handlleUpdate}>Cập nhật</button>
                      </div>
                    </div>
                  </div>
                </div>)}

              </Popup>
              <div id="menu-top">
                <div id="iconchat" className="menu chat activemenu">
                  <Link to={`/c/${idchat}/chat/slide`}>
                    <i className="far fa-envelope" />
                  </Link>
                </div>
                <div id="iconfriend" className="menu friend">
                  <Link to={`/c/${idchat}/friend`}>
                    <i className="far fa-address-book" />
                  </Link>
                </div>
                <div id="iconfavourite" className="menu favourite">
                  <Link to={`/c/${idchat}/favourite`}>
                    <i className="far fa-heart" />
                  </Link>
                </div>
              </div>
              <Popup position="top center" closeOnDocumentClick
                trigger={<div id="menu-bottom">
                  <div className="menu seting" onClick={() => handleOpen}>
                    <i className="fas fa-cog" />
                  </div>
                </div>}>

                {close => (<div id="smenu">
                  <div id="smenu-in">
                    <Popup modal nested
                      trigger={<div className="smenu-item introduce-click">
                        Giới thiệu
                     </div>}>

                      {close => (<div id="introduce-in">
                        <div className="introduce-header">
                          <div className="name-header">
                            Fchat web
          </div>
                          <div className="img-introduce">
                            <img src="/images/logo1.png" alt="" />
                          </div>
                        </div>
                        <div className="introduce-content">
                          <div className="itr-ct-left">
                            <div>
                              <div>Phát triển bởi</div>
                              <div style={{ fontSize: '14px', fontWeight: 500 }}>Vũ Văn Thắng</div>
                            </div>
                            <div>
                              <div style={{ fontSize: '16px', fontWeight: 650 }}>VNV online</div>
                              <div>Đội 10, Quỳnh Huê, Thống Nhất, Gia Lộc, Hải Dương</div>
                            </div>
                          </div>
                          <div className="itr-ct-right">
                            <div>
                              <div>Liên hệ</div>
                              <div style={{ fontWeight: 500 }}>Admin</div>
                            </div>
                            <div style={{ marginTop: '5px' }}>
                              <div>
                                <span style={{ fontWeight: 500 }}>Số điện thoại:</span>
                0866840599
              </div>
                              <div>
                                <span style={{ fontWeight: 500 }}>Email:</span>
                vuthangit2000@gmail.com
              </div>
                              <div>
                                <span style={{ fontWeight: 500 }}>facebook:</span>
                                <a href="https://www.facebook.com/vu.thang.90834" target="_blank" style={{ color: 'blue' }}>Anh Thắng đz</a>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>)}
                    </Popup>
                    <div className="smenu-line" />
                    <div className="smenu-item" style={{ color: 'red' }}>
                      Đăng xuất
          </div>
                    <div className="smenu-item" onClick={() => close()}>
                      Thoát
          </div>
                  </div>
                </div>)}

              </Popup>
            </div>
          </div>
        </div>


      </div>
    </div>
  );
}

export default Chatdefault;
