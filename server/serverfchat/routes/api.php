<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post('/login', 'App\Http\Controllers\CustomerController@login');
Route::post('/loginadmin', 'App\Http\Controllers\AdminController@loginAdmin');
Route::get('/Getuser/{id}', 'App\Http\Controllers\CustomerController@GetInfor');
Route::post('/updateuser', 'App\Http\Controllers\CustomerController@updateUser');
Route::get('/getfriend/{id}', 'App\Http\Controllers\CustomerController@GetFriend');
Route::post('/findfriend', 'App\Http\Controllers\FriendController@findFriend');
Route::get('/getInforfriend/{UserId}&{FriendId}', 'App\Http\Controllers\FriendController@getInfofriend');
Route::get('/getMessage/{UserId}&{FriendId}', 'App\Http\Controllers\MessageController@getMessage');
Route::post('/updatenickname', 'App\Http\Controllers\FriendController@updateNickName');
Route::get('/statistical', 'App\Http\Controllers\AdminController@statistical');