<?php

namespace App\Http\Controllers;

use App\Models\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function statistical()
    {
        $customer = DB::table('customer')->get()->count();
        $admin = DB::table('admin')->get()->count();
        $message = DB::table('message')->get()->count();

        return response()->json([
            "user" => $customer,
            "admin" => $admin,
            "message" => $message
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }


    public function loginAdmin(Request $request)
    {
        $request->validate([            
            'Email' => 'required',
            'Password' => 'required' 
        ]);
        $login = DB::table('admin')->where('Email',$request->Email)->where('Password',$request->Password)->get();
        if($login == null){
            return response()->json("");
        }
        else{
            $Status='Sucesfully';            
            return response()->json(
                [
                    'Status' =>'Seccesfully',
                    'datas' => $login
                ]
            );
        }   
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function show(Admin $admin)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function edit(Admin $admin)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Admin $admin)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function destroy(Admin $admin)
    {
        //
    }
}
