<?php

namespace App\Http\Controllers;

use App\Models\customer;
use App\Models\Friend;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $expenses = customer::all();
        return response()->json($expenses);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {        
        $request->validate([            
            'Email' => 'required',
            'Password' => 'required' 
        ]);
        $login = DB::table('customer')->where('Email',$request->Email)->where('Password',$request->Password)->get();
        if($login == null){
            return response()->json("");
        }
        else{
            $Status='Sucesfully';            
            return response()->json(
                [
                    'Status' =>'Seccesfully',
                    'datas' => $login
                ]
            );
        }        
    }
    public function GetInfor(Request $request)
    {
        
        $get = DB::table('customer')->where('Id', $request->id)->get();
        return response()->json($get);
    }

    public function GetFriend(Request $request)
    {
        
        $get = DB::table('customer')
            ->join('friend','customer.Id','=','friend.FriendId')
            ->where('friend.UserId', $request->id)
            ->get();
        return response()->json($get);
    }    

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function show(customer $customer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function edit(customer $customer)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function updateUser(Request $request, customer $customer)
    {
        
        $request->validate([
            'Id' => 'required',                     
            'address' => 'required'                     
        ]);    
        $get = DB::table('customer')->where('Id', $request->Id)
                                    ->update(['address' => $request->address]);                     
        
        return response()->json([
            'Status' => 'Sucesfully'            
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function destroy(customer $customer)
    {
        //
    }
}
